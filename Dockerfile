FROM ubuntu:20.04

COPY requirements.txt /

RUN apt-get -y update && \
    apt-get -y install --no-install-recommends software-properties-common && \
    add-apt-repository -y ppa:stebbins/handbrake-releases && \
    apt-get -y update && \
    apt-get -y install --no-install-recommends \
      handbrake-cli python3-pip python3-setuptools ffmpeg rsync \
      mediainfo=19.09-1build1 && \
    pip3 install -r /requirements.txt && \
    mkdir /scan && \
    rm -rf /var/lib/apt/lists/*

VOLUME /scan
VOLUME /scratch

COPY compress.py /
RUN chmod 755 compress.py

ENTRYPOINT ["/compress.py", "/scan"]
