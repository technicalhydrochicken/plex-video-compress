#!/usr/bin/env python3
import os
import sys
import subprocess
import humanize
import logging
from datetime import timedelta, datetime
import re
from optparse import OptionParser
import json

parser = OptionParser()
parser.add_option('-c', '--compress', dest='compress', action='store_true')
parser.add_option('-i', '--ignore', dest='ignore_pattern')
parser.add_option('-v', '--verbose', action='store_true')
parser.add_option('-l',
                  '--copy-local',
                  action='store_true',
                  help="Copy movie to local filesystem before compressing")
parser.add_option('-k',
                  '--keep-original',
                  action='store_true',
                  help="Keep original file")
(options, args) = parser.parse_args()

if options.verbose:
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
else:
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
compress = options.compress
ignore_pattern = options.ignore_pattern

if len(args) != 1:
    sys.stderr.write("Usage: %s target_dir\n" % sys.argv[0])
    sys.exit(2)

BASE_DIR = args[0]

# def shellquote(s):
#   import pipes
#   return pipes.quote(s)


def is_movie(possible_movie):
    #   movie_exts = [
    #       '.ts', '.mp4', '.avi', '.mkv', '.iso', '.img', '.wmv', '.mov']
    movie_exts = [
        b'.ts', b'.mp4', b'.mkv', b'.iso', b'.img', b'.wmv', b'.mov', b'.avi'
    ]
    basefile, ext = os.path.splitext(possible_movie)
    ext = ext.lower()

    logging.debug(f"Checking to see if {ext} is in {movie_exts}")
    if ext in movie_exts:
        return True
    else:
        return None


def touch(filepath):
    open(filepath, 'a').close()


def get_avinfo(filename):

    #   filename = shellquote(filename)

    raw_info = subprocess.check_output(
        ["mediainfo", "--output=JSON", filename])
    info = json.loads(raw_info)['media']
    track_info = {}
    for track in info['track']:
        if track['@type'] in track_info:
            track_info[track['@type']] += 1
        else:
            track_info[track['@type']] = 1
    info['track_info'] = track_info
    return info


def get_compressed_marker(full_path):

    base, orig_filename = os.path.split(full_path)
    new_filename = b".compressed." + orig_filename + b".marked"
    compressed_marker = os.path.join(base, new_filename)
    return compressed_marker


def main():
    all_video_files = []
    report_interval = 25

    for filename in subprocess.check_output(["find", BASE_DIR, "-type",
                                             "f"]).split(b"\n"):
        filename = filename.strip()
        if filename == b"":
            continue

        logging.debug(f"Examining {filename} for compression")

        # Is this a normalization file?
        if filename.endswith(b"normalizing"):
            logging.info("Skipping %s since it's normalizing" % filename)
            continue

        # Did we already compress this?
        compressed_marker = get_compressed_marker(filename)
        logging.debug("Checking for existince of %s" %
                      compressed_marker.decode())
        if os.path.exists(compressed_marker):
            logging.debug(
                f"Skipping {filename}, since {compressed_marker} exists")
            continue

        # Skip
        if ignore_pattern and re.match(
                ignore_pattern, filename, flags=re.IGNORECASE):
            logging.debug(
                f"Skipping {filename} because it matches {ignore_pattern}")
            continue

        # Completed file
        completed_file = "%s/.compression_completed" % os.path.dirname(
            filename)

        if os.path.exists(completed_file):
            logging.info(
                f"Skipping {filename} because {completed_file} exists")
            continue

        if is_movie(filename):
            logging.debug(f"Adding {filename} to video file list")
            all_video_files.append(filename)
        else:
            logging.debug(f"{filename} is not a movie file")

    # all_video_files_count = len(all_video_files)

    compressable_video_files = []
    for filename in all_video_files:
        compressable_video_files.append(filename)

    compressable_video_files.sort()
    # Unique these bitches up
    compressable_video_files_count = len(compressable_video_files)

    if compressable_video_files_count > 0:
        logging.warning("About to compress the following files")
        for i in range(compressable_video_files_count):
            marker = "%s:" % (i + 1)
            logging.warning("%-3s %s" %
                            (marker, compressable_video_files[i].decode()))

    for i in range(compressable_video_files_count):
        filename = compressable_video_files[i]

        orig_avinfo = get_avinfo(filename)
        # TODO:  Fix this
        basedir, original_filename = os.path.split(filename)
        original_filename_base, original_extension = os.path.splitext(
            original_filename)

        transitional_filename = b"%s.mkv.normalizing" % original_filename_base
        transitional_filename_full = os.path.join(basedir,
                                                  transitional_filename)

        post_filename = b"%s%s" % (original_filename_base, b'.mkv')
        post_filename_full = os.path.join(basedir, post_filename)

        cmd_parts = [
            b'HandBrakeCLI',
            b'--json',
            b'--encoder',
            b'x264',
            b'--format',
            b'av_mkv',
            b'--main-feature',
            b'--preset',
            b'Roku 720p30 Surround',
            b"--subtitle",
            ','.join(
                str(x)
                for x in range(1, orig_avinfo['track_info'].get('Text', 10) +
                               1)).encode(),
            b"--audio",
            ','.join(
                str(x)
                for x in range(1, orig_avinfo['track_info'].get('Audio', 10) +
                               1)).encode(),
            b'-i',
            filename,
            b'-o',
            transitional_filename_full,
        ]
        cmd = b" ".join(cmd_parts)
        logging.debug(cmd)
        original_size = os.path.getsize(filename)
        if compress:
            logging.warning("Compressing %s (%s of %s)" %
                            (filename, i + 1, compressable_video_files_count))
            logging.debug(cmd)

        else:
            logging.warning(f"Going to compress {filename}")

        # Actual compression
        if compress:

            start_time = datetime.now()
            proc = subprocess.Popen(cmd_parts,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE,
                                    bufsize=1,
                                    universal_newlines=True)

            while (not proc.poll()) and (proc.poll() != 0):
                eta_seconds = None
                pct_done = None
                last_report_marker = 0
                for line in iter(proc.stdout.readline, ''):
                    if '"ETASeconds"' in line:
                        eta_seconds = int(
                            line.split(":")[1].split(",")[0].strip())
                    if '"Progress"' in line:
                        raw_pct = line.split(":")[1].split(",")[0].strip()
                        pct_done = int(float(raw_pct) * 100)

                    if (pct_done and eta_seconds) and ((pct_done %
                                                        report_interval) == 0):
                        if last_report_marker != pct_done:
                            time_left = timedelta(seconds=eta_seconds)
                            logging.warning("%s%% | %s" %
                                            (pct_done, time_left))
                            last_report_marker = pct_done

            if proc.poll() != 0:
                logging.error("Process compression failed")
                return 5

            if not os.path.exists(transitional_filename_full):
                logging.error(
                    "New file is not created...where'd it go?  Quitting...\n")
                sys.exit(2)

            end_time = datetime.now()
            marker_file = get_compressed_marker(post_filename_full)
            touch(marker_file)

            new_size = os.path.getsize(transitional_filename_full)
            logging.info("Compressed from %s to %s in %s" %
                         (humanize.naturalsize(original_size),
                          humanize.naturalsize(new_size),
                          humanize.naturaldelta(end_time - start_time)))

            if not options.keep_original:
                os.remove(filename)
                os.rename(transitional_filename_full, post_filename_full)
                compressed_marker = get_compressed_marker(filename)
                touch(compressed_marker)
    print("Task complete")


if __name__ == '__main__':
    sys.exit(main())
