#!/bin/sh
set -x
set -e

rm -f sample/*.mkv
rm -f sample/.compressed*
cp ~/movie_samples/* sample/
docker build . -t video-compress && docker run -ti -v $PWD/sample:/scan --rm video-compress -v -c
